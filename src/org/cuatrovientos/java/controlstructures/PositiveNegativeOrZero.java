package org.cuatrovientos.java.controlstructures;

import java.util.Scanner;

public class PositiveNegativeOrZero {
	/*
	 * Ejercicio 2: Crea un proyecto con una clase llamada PositiveNegativeOrZero
	 * que solicite al usuario un valor entero y muestre por pantalla si ese numero
	 * es positivo, negativo o 0.
	 * 
	 */
	private static Scanner sc;

	public static void ejercicio2() {
		int x;
		sc = new Scanner(System.in);
		System.out.println("Introduzca un numero entero:");
		x = Integer.parseInt(sc.nextLine());
		if (x < 0) {
			System.out.println("El mumero " + x + " es negativo");
		} else if (x > 0) {
			System.out.println("El mumero " + x + " es positivo");
		} else {
			System.out.println("El mumero " + x + " es cero");
		}

	}
}
