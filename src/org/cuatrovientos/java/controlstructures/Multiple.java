package org.cuatrovientos.java.controlstructures;

import java.util.Scanner;

public class Multiple {
	/*
	 * Ejercicio 5: Crea un proyecto con una clase llamada Multiple que solicite al
	 * usuario dos valores enteros y muestre por pantalla si el primero es multiplo
	 * del segundo.
	 */
	private static Scanner sc;

	public static void ejercicio5() {
		int x, y;
		sc = new Scanner(System.in);
		System.out.println("Introduzca dos numeros enteros:");
		x = sc.nextInt();
		y = sc.nextInt();
		if (x == 0 || y == 0) {
			System.out.println("El valor 0 no es valido");
		} else {
			if (x % y == 0) {
				System.out.println("El mumero " + x + " es multiplo del numero " + y);
			} else {
				System.out.println("El mumero " + x + " no es multiplo del numero " + y);
			}
		}
	}

}
