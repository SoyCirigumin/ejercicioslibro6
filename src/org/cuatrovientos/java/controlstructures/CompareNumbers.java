package org.cuatrovientos.java.controlstructures;

import java.util.Scanner;

public class CompareNumbers {
	/*
	 * Ejercicio 4: Crea un proyecto con una clase llamada CompareNumbers que
	 * solicite al usuario dos valores enteros, los compare y muestre por pantalla
	 * si uno es mayor que el otro o si son iguales.
	 */
	private static Scanner sc;

	public static void ejercicio4() {
		int x, y;
		sc = new Scanner(System.in);
		System.out.println("Introduzca dos numeros enteros:");
		x = Integer.parseInt(sc.nextLine());
		y = Integer.parseInt(sc.nextLine());
		if (x < y) {
			System.out.println("El mumero " + x + " es menor que el numero " + y);
		} else if (x > y) {
			System.out.println("El mumero " + x + " es mayor que el numero " + y);
		} else {
			System.out.println("El mumero " + x + " es igual que el numero " + y);
		}
	}

}
