package org.cuatrovientos.java.controlstructures;

import java.util.Scanner;

public class LanguageMessage {
	/*
	 * Ejercicio 7: Crea un proyecto con una clase llamada LanguageMessage
	 * que solicite al usuario un nombre de lenguaje y saque un mensaje distinto segun el nombre de ese mensaje:
	 * 		Si el lenguaje es "C++" el mensaje a sacar sera: "The best language ever".
	 * 		Si el lenguaje es "java" el mensaje a sacar sera: "The second best language ever".
	 * 		Si el lenguaje es "javascript" el mensaje a sacar sera: "The language of the present".
	 * 		Si es cualquier otro debe decir "Nothing to say about that".
	 */
	private static Scanner sc;
	public static void ejercicio7() {
		String lenguaje;
		sc = new Scanner(System.in);
		System.out.println("Introduzca el nombre del lenguaje");
		lenguaje = sc.nextLine().toLowerCase();
		// Lo hacemos con un Swicht
		System.out.println("Primero con un Switch");
		switch (lenguaje) {
		case "c++": {
			System.out.println("The best language ever");
			break;
		}
		case "java": {
			System.out.println("The second best language ever");
			break;
		}
		case "javascript": {
			System.out.println("The language of the present");
			break;
		}
		default:
			System.out.println("Nothing to say about that");
			break;
		}
		
		// Lo hacemos con if's
		System.out.println("Ahora con if's");
		if (lenguaje.equalsIgnoreCase("c++")) {
			System.out.println("The best language ever");
		}
		if (lenguaje.equalsIgnoreCase("java")) {
			System.out.println("The language of the present");
		}
		if (lenguaje.equalsIgnoreCase("javascript")) {
			System.out.println("The language of the present");
		}
		if (!lenguaje.equalsIgnoreCase("c++") && !lenguaje.equalsIgnoreCase("java")
				&& !lenguaje.equalsIgnoreCase("javascript")) {
			System.out.println("Nothing to say about that");
		}

	}
}
