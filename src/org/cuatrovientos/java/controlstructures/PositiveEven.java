package org.cuatrovientos.java.controlstructures;

import java.util.Scanner;

public class PositiveEven {
	/*
	 * Ejercicio 3: Crea un proyecto con una clase llamada PositiveEven que solicite
	 * al usuario un numero entero y muestre por pantalla si ese numero es par y
	 * positivo. En caso contrario debe indicar si es negativo, impar o ambos.
	 		Introduzca un numero entero:
			-3
			El mumero -3 es negativo e impar
	 */
	private static Scanner sc;

	public static void ejercicio3() {
		int x;
		sc = new Scanner(System.in);
		System.out.println("Introduzca un numero entero:");
		x = Integer.parseInt(sc.nextLine());
		if (x < 0) {
			if (x % 2 == 0) {
				System.out.println("El mumero " + x + " es negativo y par");
			} else {
				System.out.println("El mumero " + x + " es negativo e impar");
			}
		} else if (x > 0) {
			if (x % 2 == 0) {
				System.out.println("El mumero " + x + " es positivo y par");
			} else {
				System.out.println("El mumero " + x + " es positivo e impar");
			}
		} else {
			System.out.println("El mumero " + x + " es cero");
		}

	}

}
