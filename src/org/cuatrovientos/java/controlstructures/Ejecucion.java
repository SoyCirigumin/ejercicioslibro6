package org.cuatrovientos.java.controlstructures;

public class Ejecucion {

	public static void main(String[] args) {
		System.out.println("Ejercicio1:");
		OddOrEven.ejercicio1();
		System.out.println("Ejercicio2:");
		PositiveNegativeOrZero.ejercicio2();
		System.out.println("Ejercicio3:");
		PositiveEven.ejercicio3();
		System.out.println("Ejercicio4:");
		CompareNumbers.ejercicio4();
		System.out.println("Ejercicio5:");
		Multiple.ejercicio5();
		System.out.println("Ejercicio6:");
		MassBodyIndexDiagnostic.ejercicio6();
		System.out.println("Ejercicio7:");
		LanguageMessage.ejercicio7();
		System.out.println("Ejercicio8:");
		Position.ejercicio8();
		System.out.println("Ejercicio9:");
		CurrencyConverter.ejercicio9();
	}

}
