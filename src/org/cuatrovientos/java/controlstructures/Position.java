package org.cuatrovientos.java.controlstructures;

import java.util.Scanner;

public class Position {
	/*
	 * Ejercicio 8: Crea un proyecto con una clase llamada Position que solicite al
	 * usuario un dorsal de jugador y haga lo siguiente: comprobar que ese numero
	 * esta entre 0 y 99. Y a continuacion el programa debe mostrar un texto con la
	 * posicion que corresponde a cada dorsal: 
	 * 		Si el usuario ha tecleado 1 el texto sera "Keeper"
	 * 		Si el usuario ha tecleado 3,4,5 el texto sera "Defender"
	 * 		Si el usuario ha tecleado 6,8,11 el texto sera "Midfield"
	 * 		Si el usuario ha tecleado 9 el texto sera "Striker"
	 * 		Para cualquier otra opcion el texto sera "Any"
	 */
	private static Scanner sc;

	public static void ejercicio8() {
		int dorsal;
		sc = new Scanner(System.in);
		System.out.println("Introduzca el dorsal del jugador entre 0 y 99");
		dorsal = Integer.parseInt(sc.nextLine());
		// Lo hacemos con un Swicht
		System.out.println("Primero con un Switch");
		if ((dorsal < 0) || (dorsal > 99)) {
			System.out.println("El numero " + dorsal + " no es valido");
		} else {
			switch (dorsal) {
			case 1: {
				System.out.println("Keeper");
				break;
			}
			case 3:
			case 4:
			case 5: {
				System.out.println("Defender");
				break;
			}
			case 6:
			case 8:
			case 11: {
				System.out.println("Midfield");
				break;
			}
			case 9: {
				System.out.println("Striker");
				break;
			}
			default:
				System.out.println("Any");
				break;
			}
		}
		// Lo hacemos con if's
		System.out.println("Ahora con if's");
		if ((dorsal < 0) || (dorsal > 99)) {
			System.out.println("El numero " + dorsal + " no es valido");
		} else {
			if (dorsal == 1) {
				System.out.println("Keeper");
			}
			if (dorsal == 3 || dorsal == 4 || dorsal == 5) {
				System.out.println("Defender");
			}
			if (dorsal == 6 || dorsal == 8 || dorsal == 11) {
				System.out.println("Midfield");
			}
			if (dorsal == 9) {
				System.out.println("Striker");
			}
			if (dorsal != 1 && dorsal != 3 && dorsal != 4 && dorsal != 5 && dorsal != 6 && dorsal != 8
					&& dorsal != 11 & dorsal != 9) {
				System.out.println("Any");
			}
		}
	}
}
