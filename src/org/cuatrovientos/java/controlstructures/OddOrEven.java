package org.cuatrovientos.java.controlstructures;

import java.util.Scanner;

public class OddOrEven {
	/*
	 * Ejercicio 1: Crea un proyecto con una clase llamada OddOrEven que solicite al
	 * usuario un numero entero y muestre por pantalla si ese numero es par o impar.
	 * Usa el operando % para comprobar si ese numero es par o impar.
	 */
	private static Scanner sc;

	public static void ejercicio1() {
		int x;
		sc = new Scanner(System.in);
		System.out.println("Introduzca un numero entero:");
		x = Integer.parseInt(sc.nextLine());
		if (x % 2 == 0) {
			System.out.println("El mumero " + x + " es par");
		} else {
			System.out.println("El mumero " + x + " es impar");
		}
	}
}
