package org.cuatrovientos.java.controlstructures;

import java.util.Scanner;

public class CurrencyConverter {
	/*
	 * Ejercicio 9: Crea un proyecto con una clase llamada CurrencyConverter que
	 * solicite al usuario una cantidad monetaria y un caracter d, p, y (dollar,
	 * pound, yen). Segun el caracter introducido por el usuario el programa debe
	 * convertir la cantidad monetaria (que seran euros) a la moneda
	 * correspondiente.
	 */
	private static Scanner sc;

	public static void ejercicio9() {
		int cantidad;
		String divisa;
		final double CAMBIO_DOLAR_EURO = 0.8446;
		final double CAMBIO_LIBRA_EURO = 1.2346;
		final double CAMBIO_YEN_EURO = 2.2346;
		sc = new Scanner(System.in);
		System.out.println("Introduzca La cantidad para convertir");
		cantidad = Integer.parseInt(sc.nextLine());
		System.out.println("Introduzca la divisa d=Dolar l=Libra Y=yen");
		divisa = sc.nextLine().toLowerCase();
		// Lo hacemos con un Swicht
		System.out.println("Primero con un Switch");

		switch (divisa) {
		case "d": {
			System.out.println("la cantidad " + cantidad + " en Dolares corresponden a " + cantidad * CAMBIO_DOLAR_EURO
					+ " Euros");
			break;
		}
		case "l": {
			System.out.println(
					"la cantidad " + cantidad + " en Libras corresponden a " + cantidad * CAMBIO_LIBRA_EURO + " Euros");
			break;
		}
		case "y": {
			System.out.println(
					"la cantidad " + cantidad + " en Yenes corresponden a " + cantidad * CAMBIO_YEN_EURO + " Euros");
			break;
		}
		default:
			System.out.println("La divisa no es valida");
			break;
		}

		// Lo hacemos con if's
		System.out.println("Ahora con if's");

		if (divisa.equalsIgnoreCase("d")) {
			System.out.println("la cantidad " + cantidad + " en Dolares corresponden a " + cantidad * CAMBIO_DOLAR_EURO
					+ " Euros");
		}
		if (divisa.equalsIgnoreCase("l")) {
			System.out.println(
					"la cantidad " + cantidad + " en Libras corresponden a " + cantidad * CAMBIO_LIBRA_EURO + " Euros");
		}
		if (divisa.equalsIgnoreCase("y")) {
			System.out.println(
					"la cantidad " + cantidad + " en Yenes corresponden a " + cantidad * CAMBIO_YEN_EURO + " Euros");
		}
		if (!divisa.equalsIgnoreCase("d") && !divisa.equalsIgnoreCase("l") && !divisa.equalsIgnoreCase("y")) {
			System.out.println("La divisa no es valida");
		}
	}
}
