package org.cuatrovientos.java.controlstructures;

import java.util.Scanner;

public class MassBodyIndexDiagnostic {
	/*
	 * Ejercicio 6: Crea un proyecto con una clase llamada MassBodyIndexDiagnostic
	 * que solicite al usuario su peso en kilos y su altura en centimetros y calcule
	 * el IMC (peso/altura´2); debe mostrar el resultado y luego hacer el
	 * diagnostico:
	 * 		Si el IMC es menor que 16 se muestra el mensaje: "You need to eat more".
	 * 		Si el IMC esta entre (>=)16 y 25(<) se muestra el mensaje: "You are fine".
	 * 		Si el IMC esta entre 25 y 30(<) se muestra el mensaje: "You are eating too much".
	 * 		Si el IMC es superior a 30 se muestra el mensaje: "Go to the hospital".
	 */
	private static Scanner sc;

	public static void ejercicio6() {
		float peso, altura, masa;
		sc = new Scanner(System.in);
		System.out.println("Introduzca el peso en Kilos");
		peso = Float.parseFloat(sc.nextLine());
		System.out.println("Introduzca la altura en Centimetros");
		altura = Float.parseFloat(sc.nextLine());
		masa = (float) (peso / Math.pow(altura, 2));
		System.out.println("Su Indice de Masa Corporal es: " + masa);
		if (masa < 16) {
			System.out.println("You need to eat more");
		} else if (masa >= 16 && masa < 25) {
			System.out.println("You are fine");
		} else if (masa >= 25 && masa < 30) {
			System.out.println("You are eating too much");
		} else {
			System.out.println("Go to the hospital");
		}

	}
}
